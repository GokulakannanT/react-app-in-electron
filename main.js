
const {app, BrowserWindow} = require('electron')
const path = require('path')
const http = require('http');
const nStatic = require('node-static');
const fileServer = new nStatic.Server('./build');

http.createServer((req, res) => {
    fileServer.serve(req, res);
}).listen(5000);

function createWindow () {
  const mainWindow = new BrowserWindow({
    width: 800,
    height: 600,
    webPreferences: {
      // preload: path.join(__dirname, 'preload.js')
    }
  })
  mainWindow.loadURL("http://localhost:5000/")
  // Open the DevTools.
  // mainWindow.webContents.openDevTools()
}
app.on('ready', createWindow)

app.on('window-all-closed', function () {
  if (process.platform !== 'darwin') app.quit()
})

app.on('activate', function () {
  if (BrowserWindow.getAllWindows().length === 0) createWindow()
})
